import unittest
try:
    from unittest import mock
except ImportError:
    import mock
from gateway_client.image import ImageAPIClient
from gateway_client import FormField
from . import BaseApiTestCase
import json
from gateway_client.client3 import to_graphql

API_RESPONSE = '''{"form_fields": {
        "url": { "type": "CharField", "kwargs": { "required": true } }
    }}'''


class ImageAPIClientTestCase(BaseApiTestCase):
    def setUp(self):
        self.patcher = mock.patch('gateway_client.requests.get')
        self.patcher2 = mock.patch('gateway_client.requests.post')
        self.patcher4 = mock.patch('gateway_client.requests.patch')
        self.pather3 = mock.patch(
            'gateway_client.requests.options')
        self.mock_get = self.patcher.start()
        self.mock_post = self.patcher2.start()
        self.mock_options = self.pather3.start()
        self.mock_patch = self.patcher4.start()
        self.instance = ImageAPIClient('http://localhost:6000/')
        self.image_service_url = 'http://localhost:6000'
        self.response = {
            "image" : {
                "url": "http://image.jpg"
            }
        }
        self.client_payload = {"data": "base64Data"}

    def tearDown(self):
        self.patcher.stop()
        self.patcher2.stop()
        self.pather3.stop()
        self.patcher4.stop()

    def test_get_image(self):
        self.mock_post.return_value = self.mock_response(self.response)
        result = self.instance.get_image('randomId')
        self.mock_post.assert_called_once_with(
            "{0}/graphql".format(self.image_service_url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.image_query()),
                'operationName': "getImage",
                'variables': {'object_id': 'randomId'},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, self.response['image'])

    def test_form_parameters_is_successfully_fetched(self):
        self.mock_options.return_value = self.mock_response(
            json.loads(API_RESPONSE), overwrite=True)
        result = self.instance.get_form_parameters()
        self.assertIsInstance(result, FormField)
        self.assertListEqual(
            sorted(result.get_fields()),
            sorted(['url']))

    def test_validation_sends_required_params(self):
        self.instance.validate(
            dict(self.client_payload))
        self.mock_post.assert_called_once_with(
            '{0}/images/validate_form/'.format(self.image_service_url),
            params={}, json=self.client_payload)

    def test_form_can_be_saved_successfully(self):
        self.mock_post.return_value = self.mock_response(
            self.response, overwrite=True)
        result = self.instance.save_form(
            dict(self.client_payload))
        self.assertEqual(result, self.response)

    def test_saving_form_sends_required_parameters(self):
        
        self.mock_post.return_value = self.mock_response(
            self.response, overwrite=True)

        result = self.instance.save_form(self.client_payload)
        self.mock_post.assert_called_once_with(
            '{0}/images/save_form/'.format(self.image_service_url),
            params={},
            json=self.client_payload)
        self.assertEqual(result, self.response)
