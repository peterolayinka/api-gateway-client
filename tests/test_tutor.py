from . import mock, MockRequest, BaseApiTestCase
from gateway_client.tutor import TutorAPIClient as TutorService
import requests
import datetime
import json
from gateway_client.client3 import to_graphql

RESULT = {
    'form_fields': {
        'form': {
            'first_name': {
                'type': 'CharField',
                'kwargs': {'required': True,
                           'label': "First name"}
            },
            'last_name': {
                'kwargs': {'required': True,
                           'label': "Last name"},
                'type': 'CharField'
            },
            'username': {
                'kwargs': {'required': False,
                           'label': "Username"},
                'type': 'CharField'
            }
        },
        'profile_form': {
            'description': {
                'kwargs': {'required': False,

                           'label': "Describe Yourself to the Tuteria Community"},
                'type': 'CharField',
            },
            'dob': {
                'kwargs': {
                    'required': True,
                    'label': "Birth Date",
                    'widget': "SelectDateWidget",
                    'kwargs': {
                        'years': tuple([y for y in range(1920, datetime.date.today().year + 1)])
                    }
                },
                'type': 'DateField'
            },
            'gender': {
                'kwargs': {
                    'label': "I am",
                    'choices': [('', 'Gender'), ('M', 'Male'), ('F', 'Female')],
                    'required': True},
                'type': 'TypedChoiceField'
            }
        },
        'phone_form': {
            'number': {
                'kwargs': {'label': 'Number', 'required': True},
                'type': 'PhoneNumberField'},
            'primary_phone_no1': {
                'kwargs': {'label': 'Confirm Primary Number',
                           'required': True},
                'type': 'PhoneNumberField'}
        },
        'address_form': {
            'form': {
                'id': {
                    'type': 'IntegerField',
                    'kwargs': {'required': False}
                },
                'address': {
                    'type': 'CharField',
                    'kwargs': {'required': True, }},
                'state': {
                    'type': 'TypedChoiceField',
                    'kwargs': {
                            'required': True,
                            'choices': [
                                ['', 'Select State'],
                                ['Abia', 'Abia'], ['Abuja', 'Abuja'],
                                ['Adamawa', 'Adamawa'],
                                ['Akwa Ibom', 'Akwa Ibom'],
                                ['Anambra', 'Anambra'], ['Bayelsa', 'Bayelsa'],
                                ['Bauchi', 'Bauchi'], ['Benue', 'Benue'],
                                ['Borno', 'Borno'], [
                                    'Cross River', 'Cross River'],
                                ['Delta', 'Delta'], ['Edo', 'Edo'],
                                ['Ebonyi', 'Ebonyi'], ['Ekiti', 'Ekiti'],
                                ['Enugu', 'Enugu'],
                                ['Gombe', 'Gombe'], ['Imo', 'Imo'],
                                ['Jigawa', 'Jigawa'],
                                ['Kaduna', 'Kaduna'], ['Kano', 'Kano'],
                                ['Katsina', 'Katsina'],
                                ['Kebbi', 'Kebbi'], ['Kogi', 'Kogi'],
                                ['Kwara', 'Kwara'], ['Lagos', 'Lagos'],
                                ['Nassawara', 'Nassawara'], ['Niger', 'Niger'],
                                ['Ogun', 'Ogun'], ['Ondo', 'Ondo'], [
                                    'Osun', 'Osun'],
                                ['Oyo', 'Oyo'], ['Plateau', 'Plateau'],
                                ['Rivers', 'Rivers'], ['Sokoto', 'Sokoto'],
                                ['Taraba', 'Taraba'], ['Yobe', 'Yobe'],
                                ['Zamfara', 'Zamfara']
                            ]}
                },
                'vicinity': {
                    'type': 'CharField',
                    'kwargs': {
                            'required': True,
                    }},
                'latitude': {
                    'type': 'DecimalField',
                    'kwargs': {'required': False, }},
                'longitude': {
                    'type': 'DecimalField',
                    'kwargs': {'required': False, }
                }
            },
            'extra': 1,
            'max_num': 1
        },
        'education_formset': {
            'form': {
                'school': {'type': 'CharField',
                           'kwargs': {'required': True, 'label': 'School'}},
                'course': {'type': 'CharField',
                           'kwargs': {'required': True, 'label': 'Course'}},
                'degree': {'type': 'TypedChoiceField',
                           'kwargs': {
                                   'required': True,
                                   'choices': [('', 'Select'), ('B.Sc.', 'B.Sc.'),
                                               ('B.A.', 'B.A.'), ('B.Ed.', 'B.Ed.'),
                                               ('B.Eng.', 'B.Eng.'), ('B.Tech.',
                                                                      'B.Tech.'),
                                               ('Diploma', 'Diploma'), ('HND', 'HND'),
                                               ('OND', 'OND'), ('M.Sc.', 'M.Sc.'),
                                               ('LL.B', 'LL.B'), ('MBA',
                                                                  'MBA'), ('PhD', 'PhD'),
                                               ('N.C.E', 'N.C.E'), ('S.S.C.E', 'S.S.C.E')],
                                   'label': 'Degree'}
                           }
            },
            'max_num': 2,
            'extra': 1,
            'validate_max': True
        },
        'we_formset': {
            'form': {'name': {'type': 'CharField',
                              'kwargs': {'required': False, 'label': 'Organization Name'}},
                     'role': {'type': 'CharField',
                              'kwargs': {'required': False, 'label': 'Role'}},
                     'currently_work': {'type': 'BooleanField',
                                        'kwargs': {'required': False, 'label': 'Currently work'}}},
            'max_num': 2,
            'extra': 1,
            'validate_max': True
        },
        'preference_form': {
            'years_of_teaching': {
                'type': 'TypedChoiceField',
                'kwargs': {'required': False,
                           'choices': [('', 'Just starting out'),
                                       (2, 'Less than 2 years'),
                                       (5, 'Between 3 to 5 years'),
                                       (10, 'Between 6 to 10 years'),
                                       (50, 'More than 10 years')],
                           'label': 'How long have you been teaching?'}},
            'curriculum_used': {
                'type': 'MultipleChoiceField',
                'kwargs': {'required': False,
                           'choices': [('1', 'Nigerian'), ('2', 'British'), ('3', 'American')]}},
                'curriculum_explanation': {'type': 'CharField', 'kwargs': {'required': False, 'label': 'Curriculum explanation'}},
                'classes': {'type': 'MultipleChoiceField',
                            'kwargs': {'required': False,
                                       'choices': [('nursery', 'Nursery'), ('primary', 'Primary'), ('jss', 'JSS Level'), ('sss', 'SSS Level'), ('undergraduate', 'Undergraduates'), ('adult', 'Adults')],
                                       'label': 'Select classes you teach'}},
                'tutor_description': {'type': 'CharField', 'kwargs': {'required': True, 'label': 'Tutor description'}},
                'tutoring_address': {'type': 'TypedChoiceField',
                                     'kwargs': {'required': True,
                                                'choices': [('user', "Client's Location"), ('tutor', "Tutor's Location"),
                                                            ('neutral', 'Anywhere convenient')],
                                                'label': 'Tutoring address'}},
                'address_reason': {'type': 'CharField', 'kwargs': {'required': False, 'label': 'Address reason'}}
        }
    }
}


class TutorServiceTestCase(BaseApiTestCase):
    module_name = "None"

    def setUp(self):
        super(TutorServiceTestCase, self).setUp()
        self.tutor_service_url = 'http://localhost:5000'
        self.instance = TutorService(url="http://localhost:5000/")
        
        self.response = {
            "user" : {
                "id": "312",
                "first_name" : "John",
                "last_name" : "Brain"
            }
        }

        self.phonenumebers_response = {
            "user_phonenumbers" : {
                "number" : "+23470559889393",
                "primary" : True,
                "verified" : True
            }
        }


    
    def test_get_user(self):
        self.mock_post.return_value = self.mock_response(self.response)
        result = self.instance.get_user('randomUsername')
        self.mock_post.assert_called_once_with(
            "{0}/graphql".format(self.tutor_service_url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.user_query()),
                'operationName': "getUser",
                'variables': {'username': 'randomUsername'},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, self.response['user'])


    def test_get_user_phonenumbers(self):
        self.mock_post.return_value = self.mock_response(self.phonenumebers_response)
        result = self.instance.get_user_phonenumbers('randomUsername')
        self.mock_post.assert_called_once_with(
            "{0}/graphql".format(self.tutor_service_url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.user_phonenumbers_query()),
                'operationName': "getUserPhoneNumbers",
                'variables': {'username': 'randomUsername'},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, self.phonenumebers_response['user_phonenumbers'])

    
    def test_get_user_educations(self):
        response = {
            'user_educations' : {
                'school' : 'University of Lagos',
                'course' : 'Computer Science',
                'degree' : 'B.Sc.'
            }
        }
        self.mock_post.return_value = self.mock_response(response)
        result = self.instance.get_user_educations('randomUsername')
        self.mock_post.assert_called_once_with(
            "{0}/graphql".format(self.tutor_service_url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.user_educations_query()),
                'operationName': "getUserEducations",
                'variables': {'username': 'randomUsername'},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, response['user_educations'])


    def test_get_user_workexperiences(self):
        response = {
            'user_workexperiences' : {
                'name' : 'Tuteria Limited',
                'role' : 'Intern',
                'currently_work' : True
            }
        }
        self.mock_post.return_value = self.mock_response(response)
        result = self.instance.get_user_workexperiences('randomUsername')
        self.mock_post.assert_called_once_with(
            "{0}/graphql".format(self.tutor_service_url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.user_workexperiences_query()),
                'operationName': "getUserWorkExperiences",
                'variables': {'username': 'randomUsername'},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, response['user_workexperiences'])

    def test_get_user_identification(self):
        response = {
            'user_identification' : {
                'identity' : 'randomId',
                'verified' : False,
            }
        }
        self.mock_post.return_value = self.mock_response(response)
        result = self.instance.get_user_identification('randomUsername')
        self.mock_post.assert_called_once_with(
            "{0}/graphql".format(self.tutor_service_url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.user_identification_query()),
                'operationName': "getUserIdentification",
                'variables': {'username': 'randomUsername'},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        self.assertEqual(result, response['user_identification'])

    def test_form_parameters_for_first_form_is_fetched(self):
        self.mock_options.return_value = self.mock_response(
            RESULT, overwrite=True)
        result = self.instance.get_form_parameters(
            form_type="edit_profile_form")
        self.mock_options.assert_called_once_with(
            'http://localhost:5000/tutors/',
            params={'form_type': 'edit_profile_form'})
        self.assertEqual(
            result['form'].first_name[1],
            RESULT['form_fields']['form']['first_name'])

    def test_form_parameter_called_with_username(self):
        result = self.instance.get_form_parameters(
            username="john",
            form_type="edit_profile_form")
        self.mock_options.assert_called_once_with(
            'http://localhost:5000/tutors/john/',
            params={'form_type': 'edit_profile_form'})

    def test_validation_works_as_expected(self):
        response = {
            'errors': {
                'form': {
                    'last_name': ["this fiedl is required"]
                }
            }
        }
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True
        )
        result = self.instance.validate(
            {'last_name': "Biola"}, username="john",
            form_type="edit_profile_form")
        self.mock_post.assert_called_once_with(
            'http://localhost:5000/tutors/john/validate_form/',
            params={'form_type': "edit_profile_form"}, json={'last_name': "Biola"})

        self.assertEqual(result, response)

    def test_form_can_be_saved_successfully(self):
        response = {
            'last_name': "John Doe"
        }
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True)
        result = self.instance.save_form(
            dict({'account_id': "00032323323"}),
            username="john", form_type='edit_profile_form')
        self.mock_post.assert_called_once_with(
            'http://localhost:5000/tutors/john/save_form/',
            params={'form_type': 'edit_profile_form'},
            json={'account_id': "00032323323"})

        self.assertEqual(result, response)

    def test_user_information_is_saved_successfully(self):
        response = {
            'id' : '78',
            'first_name' : 'gofaniyi'
        }
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True)
        result = self.instance.create_user(
            dict({'slug': "gofaniyi", 'email' : 'gofaniyi@bounce.com', 'first_name' : 'Olaoluwa'}))
        self.mock_post.assert_called_once_with(
            'http://localhost:5000/tutors/create_user/',
            params={},
            json={'slug': "gofaniyi", 'email' : 'gofaniyi@bounce.com', 'first_name' : 'Olaoluwa'})

        self.assertEqual(result, response)

    def test_user_milestone_can_be_saved_successfully(self):
        response = {
            'id' : '78'
        }
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True)
        result = self.instance.save_user_milestone(
            dict({'description': "complete_profile"}),
            username="john")
        self.mock_post.assert_called_once_with(
            'http://localhost:5000/tutors/john/save_milestone/',
            params={},
            json={'description': "complete_profile"})

        self.assertEqual(result, response)

    def test_an_update_to_user_phonenumber_can_be_saved_successfully(self):
        response = {
            'number' : '+23471'
        }
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True)
        result = self.instance.update_user_phonenumber(
            dict({'number': "+2341", "previous_number" : "+2342", "primary" : True, "verified" : True}),
            username="john")
        self.mock_post.assert_called_once_with(
            'http://localhost:5000/tutors/john/update_phonenumber/',
            params={},
            json={'number': "+2341", "previous_number" : "+2342", "primary" : True, "verified" : True})

        self.assertEqual(result, response)

    def test_user_phonenumber_is_saved_successfully(self):
        response = {
            'number' : '+23471'
        }
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True)
        result = self.instance.save_user_phonenumber(
            dict({'number': "+2341", "primary" : True, "verified" : True}),
            username="john", form_type='phonenumber_form')
        self.mock_post.assert_called_once_with(
            'http://localhost:5000/tutors/john/save_form/',
            params={'form_type' : 'phonenumber_form'},
            json={'number': "+2341", "primary" : True, "verified" : True})

        self.assertEqual(result, response)


    def test_user_location_is_saved_successfully(self):
        response = {
            'address' : 'Baruwa street'
        }
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True)
        result = self.instance.save_user_location(
            dict({'address' : 'Baruwa street', 'state' : 'Lagos', 'vicinity' : 'Shomolu'}),
            username="john", form_type='location_form')
        self.mock_post.assert_called_once_with(
            'http://localhost:5000/tutors/john/save_form/',
            params={'form_type' : 'location_form'},
            json={'address' : 'Baruwa street', 'state' : 'Lagos', 'vicinity' : 'Shomolu'})

        self.assertEqual(result, response)

    def test_user_education_can_be_saved_successfully(self):
        response = {
            'id' : '78'
        }
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True)
        result = self.instance.save_user_educations(
            [{'school': "University of Lagos", "degree" : "B.Sc.", "course" : "Computer Science"}],
            username="john")
        self.mock_post.assert_called_once_with(
            'http://localhost:5000/tutors/john/save_educations/',
            params={},
            json=[{'school': "University of Lagos", "degree" : "B.Sc.", "course" : "Computer Science"}])

        self.assertEqual(result, response)


    def test_user_workexperience_can_be_saved_successfully(self):
        response = {
            'id' : '78'
        }
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True)
        result = self.instance.save_user_workexperiences(
            [{'name': "Tuteria Limited", "role" : "Intern", "currently_work" : True}],
            username="john")
        self.mock_post.assert_called_once_with(
            'http://localhost:5000/tutors/john/save_workexperiences/',
            params={},
            json=[{'name': "Tuteria Limited", "role" : "Intern", "currently_work" : True}])

        self.assertEqual(result, response)

    
    def test_user_identification_can_be_saved_successfully(self):
        response = {
            'id' : '78'
        }
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True)
        result = self.instance.save_user_identification(
            dict({'identity': "randomId"}),
            username="john")
        self.mock_post.assert_called_once_with(
            'http://localhost:5000/tutors/john/save_identification/',
            params={},
            json={'identity': "randomId"})

        self.assertEqual(result, response)