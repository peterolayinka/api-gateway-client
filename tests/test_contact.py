import unittest
import json
from . import mock
from gateway_client.contact import SendGridAPIHelper, TuteriaApiException
from faker import Faker



class ContactAPITestCase(unittest.TestCase):
    def setUp(self):
        self.url = 'http://localhost:5000'

        self.api_helper = SendGridAPIHelper(url=self.url)

    def mock_user(self):
        person = Faker()
        return dict(
            email=person.email(),
            first_name=person.first_name(),
            last_name=person.last_name())

    def test_api_can_create_single_sendgrid_contact_without_custom_fields(self):

        contact_id = self.api_helper.create_contact_recipient(**self.mock_user())


        self.assertIsNotNone(contact_id[0])
        self.assertTrue(len(contact_id[0]) > 0)

    def test_api_cannot_create_single_sendgrid_contact_with_incomplete_data_and_without_custom_fields(self):
        user = self.mock_user()

        del user['first_name']
        with self.assertRaises(TypeError) as error:
            contact_id = self.api_helper.create_contact_recipient(
                **user)

        self.assertEqual(
            str(error.exception),
            "create_contact_recipient() missing 1 required positional argument: 'first_name'"
        )

    def test_api_cannot_create_single_contact_with_invalid_custom_fields(self):
        user = self.mock_user()

        user['invalid_custom_field'] = "invalidresult"


        contact_id = self.api_helper.create_contact_recipient(
                **user)

        self.assertIsNotNone(contact_id[0])
        self.assertTrue(len(contact_id[0]) > 0)


    def test_api_can_create_multiple_sendgrid_contacts_without_custom_fields(self):
        person = Faker()

        contacts = [
            self.mock_user(),self.mock_user(), self.mock_user(), self.mock_user()
        ]

        contact_ids = self.api_helper.create_sendgrid_contact_recipients(contacts)

        self.assertEqual(len(contact_ids),4)
        for id in contact_ids:
            self.assertIsNotNone(id)
            self.assertTrue(len(id) > 0)


    def test_api_cannot_create_multiple_sendgrid_contacts_with_incomplete_data_and_without_custom_fields(self):
        person = Faker()

        contacts = [
            self.mock_user(),
            self.mock_user(),
            self.mock_user(),
            self.mock_user()
        ]

        for x in contacts:
            del x['email']

        with self.assertRaises(TuteriaApiException) as error:
            contact_ids = self.api_helper.create_sendgrid_contact_recipients(
                contacts)

        self.assertEqual(
            str(error.exception),
            "{'error_indices': [0, 1, 2, 3], 'message': 'Email is missing.'}")

    def test_api_can_create_single_sendgrid_contact_with_custom_fields(self):
        user = self.mock_user()

        user['isTutor']= "True"
        user['completed_tutor_application'] = "True"

        contact_id = self.api_helper.create_contact_recipient(**user)

        self.assertIsNotNone(contact_id[0])
        self.assertTrue(len(contact_id[0]) > 0)


    def test_api_can_create_multiple_sendgrid_contacts_with_custom_fields(self):
        user = self.mock_user()
        user1 = self.mock_user()
        user2 = self.mock_user()

        user['isTutor'] = "True"
        user['completed_tutor_application'] = "True"
        user1['isTutor'] = "True"
        user1['completed_tutor_application'] = "True"
        user2['isTutor'] = "True"
        user2['completed_tutor_application'] = "True"

        contact_ids = self.api_helper.create_sendgrid_contact_recipients(
            [user,user1,user2])

        self.assertEqual(len(contact_ids),3)

        for id in contact_ids:
            self.assertIsNotNone(id)
            self.assertTrue(len(id) > 0)

    def test_api_cannot_create_multiple_contacts_with_invalid_custom_fields(self):
        user = self.mock_user()
        user1 = self.mock_user()
        user2 = self.mock_user()

        user['invalid_custom_field'] = "invalidresult"
        user1['invalid_custom_field'] = "invalidresult"
        user2['invalid_custom_field'] = "invalidresult"


        contact_ids= self.api_helper.create_sendgrid_contact_recipients([user,user1,user2])

        self.assertEqual(len(contact_ids),3)

        for id in contact_ids:
            self.assertIsNotNone(id)
            self.assertTrue(len(id) > 0)
