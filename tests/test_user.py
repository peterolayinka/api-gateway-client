from . import unittest
from . import mock
import graphene
from gateway_client.user import UserDetailAPIClient
from gateway_client import TuteriaApiException, GraphQLClient
from graphene.types.json import JSONString


class CustomerInputType(graphene.InputObjectType):
    email = graphene.String()
    first_name = graphene.String()
    last_name = graphene.String()
    background_check_consent = graphene.Boolean()
    number = graphene.String()
    home_address = graphene.String()
    state = graphene.String()
    longitude = graphene.String()
    latitude = graphene.String()
    vicinity = graphene.String()
    referral_code = graphene.String()


class UserDNode(graphene.ObjectType):
    primary_phone_no = graphene.String()
    id = graphene.Int()

    def resolve_id(self, *args):
        return self['id']

    def resolve_primary_phone_no(self, args, context, info):
        return self['primary_phone_no']


users = [
    {'id': 28, 'primary_phone_no': "+234803523992"}]


class CreateUserMutation(graphene.Mutation):
    class Input:
        input = CustomerInputType()

    user = graphene.Field(lambda: UserDNode)

    @staticmethod
    def mutate(root, args, context, info):
        return CreateUserMutation(user=users[0])


class UpdateEmailMutation(graphene.Mutation):
    class Input:
        ids = graphene.List(graphene.Int)
        email = graphene.String()
    users = graphene.List(lambda: UserDNode)

    @staticmethod
    def mutate(root, args, context, info):
        result = UpdateEmailMutation(users=users)
        return result


class Mutation(graphene.ObjectType):
    create_customer = CreateUserMutation.Field()
    update_email = UpdateEmailMutation.Field()


class Query(graphene.ObjectType):
    user_detail = graphene.Field(
        UserDNode, slug=graphene.String(required=True))

    def resolve_user_detail(self, *args):
        return users[0]


class BaseTestCase(object):

    def test_service_can_receive_a_schema_object(self):

        self.assertEqual(self.service.client, self.schema)

    def test_service_throws_error_when_no_url_is_passed(self):
        with self.assertRaises(TuteriaApiException):
            service = UserDetailAPIClient()

    def test_create_customer_account(self):
        result = self.service.create_customer_account({
            'email': "j@example.com",
            'first_name': 'olodo',
            'last_name': 'apata',
            'latitude': None,
            'longitude': None,
        })
        self.assertEqual(result['id'], 28)

    def test_get_user_details(self):
        result = self.service.get_user_details("aloiba")
        self.assertEqual(result['primary_phone_no'], "+234803523992")

    def test_update_email_address(self):
        result = self.service.update_email_address([2, 3], "j@example.com")
        self.assertEqual(result[0]['id'], users[0]['id'])


class LocalUserClientTestCase(BaseTestCase, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.schema = graphene.Schema(
            query=Query, mutation=Mutation, auto_camelcase=False)
        self.service = UserDetailAPIClient(self.schema)


class RemoteUserClientTestCase(BaseTestCase, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.patcher = mock.patch.object(GraphQLClient, "execute")
        self.mocker = self.patcher.start()
        ss = users[0].copy()
        self.mocker.return_value = {
            'data': {
                'user_detail': ss,
                'create_customer': {
                    'user': ss
                },
                'update_email': {
                    'users': users
                },

            }
        }
        self.service = UserDetailAPIClient("http://localhost:5000")
        self.schema = self.service.client

    def tearDown(self):
        self.patcher.stop()

    def test_instance_of_client_is_of_GraphQLClient(self):
        self.assertIsInstance(self.service.client, GraphQLClient)
        self.assertFalse(self.service.change)

    def test_create_customer_account(self):
        super().test_create_customer_account()
        self.mocker.assert_called_once_with(self.service.query, {
            'input': {
                'email': "j@example.com",
                'first_name': 'olodo',
                'last_name': 'apata',
                'latitude': None,
                'longitude': None
            }}, "CreateCustomer")

    def test_update_email_address(self):
        super().test_update_email_address()
        self.mocker.assert_called_once_with(self.service.query, {
            'ids': [2, 3],
            'email': "j@example.com"
        }, "UpdateEmailAddress")

    def test_get_user_details(self):
        super().test_get_user_details()
        self.mocker.assert_called_once_with(
            self.service.query,
            {"slug": "aloiba"}, "GetUserDetails")
