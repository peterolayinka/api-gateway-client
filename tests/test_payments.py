# pylint: disable=E1004
from __future__ import unicode_literals
from builtins import super
import six
import unittest
import json
import copy
import os
from . import mock, MockRequest, BaseApiTestCase
from dateutil import parser
from datetime import datetime
import copy

from gateway_client.payments import (
    PaymentService, TuteriaApiException, calculate_wallet_balance,
    FormField, get_field_value)

RESULT = {
    "payout_type": {
        "type": "ChoiceField",
        "kwargs": {
            "choices": [[1, "Bank Transfer"]]
        }
    },
    "account_name": {
        "type": "CharField",
        "kwargs": {
            "required": False
        }
    },
    "bank": {
        "type": "ChoiceField",
        "kwargs": {
            "choices": [
                ["", "Select Bank"],
                ["Citibank", "Citibank"],
                ["Access Bank", "Access Bank"],
                ["Diamond Bank", "Diamond Bank"],
                ["Ecobank Nigeria", "Ecobank Nigeria"],
                ["Enterprise Bank", "Enterprise Bank"],
                ["Fidelity Bank Nigeria", "Fidelity Bank Nigeria"],
                ["First Bank of Nigeria", "First Bank of Nigeria"],
                ["First City Monument Bank", "First City Monument Bank"],
                ["First Inland Bank", "First Inland Bank"],
                ["Guaranty Trust Bank", "Guaranty Trust Bank"],
                ["Heritage Bank", "Heritage Bank"],
                ["Keystone Bank Limited", "Keystone Bank Limited"],
                ["Mainstreet Bank", "Mainstreet Bank"],
                ["Skye Bank", "Skye Bank"],
                ["Stanbic IBTC Bank", "Stanbic IBTC Bank"],
                ["Standard Chartered Bank", "Standard Chartered Bank"],
                ["Sterling Bank", "Sterling Bank"],
                ["Union Bank of Nigeria", "Union Bank of Nigeria"],
                ["United Bank for Africa", "United Bank for Africa"],
                ["Unity Bank", "Unity Bank"],
                ["Wema Bank", "Wema Bank"],
                ["Zenith Bank", "Zenith Bank"]
            ],
            "required": False
        }
    },
    "account_id": {
        "type": "CharField",
        "kwargs": {
            "required": True
        }
    }
}

gateway_response = {
    "wallet": {
        "owner": "xplicit2013",
        "credits": "0.00000000",
        "amount_available": 0,
        "amount_in_session": 10000,
        "credit_in_session": 0,
        "wallet_type": "user",
        "total_earned": 0,
        "total_withdrawn": 0,
        "total_credit_used_to_hire": 0,
        "total_used_to_hire": 59998,
        "total_paid_to_tutor": 49998,
        "total_payed_to_tutor": 49998,
        "upcoming_earnings": 0,
        "authorization_code": "",
        "auto_withdraw": False,
        "total_credit_used": 0,
        "total_in_session": 10000,
        "count": 6,
        "transactions": [
            {
                "display": "TUTOR_HIRE",
                "booking_status": 1,
                "booking_user": "xplicit2013",
                "booking_tutor": "besidoneo",
                "to_string": "Payment used to hire tutor",
                "amount": 10000,
                "transaction_type": "expenditure",
                "credit": 0,
                "type": 2,
                "amount_paid": 0,
                "total": 10000,
                "modified": "2016-07-17T20:41:35.087836Z",
                "created": "2016-07-17T20:41:35.087737Z"
            },
            {
                "display": "TUTOR_HIRE",
                "booking_status": 3,
                "booking_user": "xplicit2013",
                "booking_tutor": "besidoneo",
                "to_string": "Payment used to hire tutor",
                "amount": 10000,
                "transaction_type": "expenditure",
                "credit": 0,
                "type": 2,
                "amount_paid": 10000,
                "total": 10000,
                "modified": "2016-06-06T11:18:20.815000Z",
                "created": "2016-06-06T11:18:20.815000Z"
            },
            {
                "display": "TUTOR_HIRE",
                "booking_status": 3,
                "booking_user": "xplicit2013",
                "booking_tutor": "besidoneo",
                "to_string": "Payment used to hire tutor",
                "amount": 10000,
                "transaction_type": "expenditure",
                "credit": 0,
                "type": 2,
                "amount_paid": 10000,
                "total": 10000,
                "modified": "2016-05-31T11:10:56.938000Z",
                "created": "2016-05-10T08:52:52.695000Z"
            },
            {
                "display": "TUTOR_HIRE",
                "booking_status": 3,
                "booking_user": "xplicit2013",
                "booking_tutor": "besidoneo",
                "to_string": "Payment used to hire tutor",
                "amount": 9999,
                "transaction_type": "expenditure",
                "credit": 0,
                "type": 2,
                "amount_paid": 9999,
                "total": 9999,
                "modified": "2016-03-28T11:39:46.806000Z",
                "created": "2016-03-28T11:39:46.806000Z"
            },
            {
                "display": "TUTOR_HIRE",
                "booking_status": 3,
                "booking_user": "xplicit2013",
                "booking_tutor": "besidoneo",
                "to_string": "Payment used to hire tutor",
                "amount": 10000,
                "transaction_type": "expenditure",
                "credit": 0,
                "type": 2,
                "amount_paid": 10000,
                "total": 10000,
                "modified": "2016-02-29T13:49:16.295000Z",
                "created": "2016-02-29T13:49:16.295000Z"
            },
            {
                "display": "TUTOR_HIRE",
                "booking_status": 3,
                "booking_user": "xplicit2013",
                "booking_tutor": "besidoneo",
                "to_string": "Payment used to hire tutor",
                "amount": 9999,
                "transaction_type": "expenditure",
                "credit": 0,
                "type": 2,
                "amount_paid": 9999,
                "total": 9999,
                "modified": "2016-02-08T21:07:33.825000Z",
                "created": "2016-02-08T21:07:33.825000Z"
            }
        ]
    }
}


class PaymentServiceTestCase(BaseApiTestCase):

    def setUp(self):
        self.patcher = mock.patch('gateway_client.payments.requests.get')
        self.patcher2 = mock.patch('gateway_client.payments.requests.post')
        self.mock_get = self.patcher.start()
        self.mock_post = self.patcher2.start()

    def tearDown(self):
        self.patcher.stop()
        self.patcher2.stop()


class RegularServiceTestCase(PaymentServiceTestCase):

    def test_exception_is_raised_when_payment_service_url_env_isnt_set(self):
        with self.assertRaisesRegexp(
                TuteriaApiException,
                "Url for Payment Service was not found."):
            PaymentService()

    def test_form_parameters_is_succesfully_fetched(self):
        os.environ.setdefault(
            'PAYMENT_SERVICE_URL', 'http://localhost:5000/payments')
        self.mock_get.return_value = self.mock_response(RESULT, overwrite=True)
        instance = PaymentService()
        result = instance.get_form_parameters()
        self.mock_get.assert_called_once_with(
            "http://localhost:5000/payments/forms/userpayout/")
        self.assertIsInstance(result, FormField)
        self.assertIsInstance(
            result.account_id, tuple)
        self.assertEqual(
            result.account_id[1], RESULT['account_id'])
        six.assertCountEqual(self,
            result.get_fields(),
            ['account_id', 'bank', 'account_name', 'payout_type'])

    def test_validation_works_as_expected(self):
        os.environ.setdefault(
            'PAYMENT_SERVICE_URL', 'http://localhost:5000/payments')
        response = {
            'account_id': [
                'Ensure this value has at most 10 characters (it has 11).'
            ]
        }
        self.mock_post.return_value = self.mock_response(
            response, overwrite=True)
        instance = PaymentService()
        result = instance.validate(dict({'account_id': "00032323323"}))
        self.assertEqual(result, response)

    def test_get_transaction_result_works_correctly(self):
        self.mock_post.return_value = self.mock_response(
            copy.deepcopy(gateway_response))
        instance = PaymentService(url='http:3000/localhost/payments/wallet')
        username = "besidoneo"
        result = instance.get_transaction_results(username, "payed", "orders")
        query = instance.construct_query(username, 'payed', "orders")
        self.mock_post.assert_called_once_with(
            'http:3000/localhost/payments/wallet',
            json=self.graphql_query(query)
        )
        self.assertEqual(
            result['wallet']['current_balance'],
            calculate_wallet_balance(gateway_response['wallet'], "orders"))

    def test_get_transaction_result_for_revenue_works_correctly(self):
        self.mock_post.return_value = self.mock_response(
            copy.deepcopy(gateway_response))
        instance = PaymentService(url='http:3000/localhost/payments/wallet')
        username = "besidoneo"
        result = instance.get_transaction_results(
            username, "earned", "revenues")
        query = instance.construct_query(username, 'earned', "revenues")
        self.mock_post.assert_called_once_with(
            'http:3000/localhost/payments/wallet',
            json=self.graphql_query(query)
        )
        self.assertEqual(
            result['wallet']['wallet_balance'],
            calculate_wallet_balance(gateway_response['wallet'], "revenues"))


class TransactionViewResultSetTestCase(PaymentServiceTestCase):

    def setUp(self):
        super().setUp()
        self.username = 'captainoak'
        self.patcher22 = mock.patch(
            "gateway_client.payments.PaymentService.get_transaction_results")
        self.mock_service = self.patcher22.start()
        self.new_response = copy.deepcopy(gateway_response)

    def tearDown(self):
        super().tearDown()
        self.patcher22.stop()

    def assertResponse(self, gateway_response, result):
        modified1 = gateway_response['wallet']['transactions'][0]['modified']
        modified2 = gateway_response['wallet']['transactions'][1]['modified']
        modified3 = gateway_response['wallet']['transactions'][2]['modified']
        if type(modified1) == str:
            self.assertEqual(parser.parse(modified1), result[0]['modified'])
            self.assertEqual(parser.parse(modified2), result[1]['modified'])
            self.assertEqual(parser.parse(modified3), result[2]['modified'])
        else:
            self.assertEqual(modified1, result[0]['modified'])
            self.assertEqual(modified2, result[1]['modified'])
            self.assertEqual(modified3, result[2]['modified'])

    def test_get_revenues_works_as_expected(self):
        self.update_date()
        self.mock_service.return_value = self.new_response
        instance = PaymentService(url='http:3000/localhost/payments/wallet')
        result = instance.get_revenues("besidoneo", "earned")
        self.mock_service.assert_called_once_with(
            'besidoneo', 'earned', "revenues")
        self.assertResponse(gateway_response, result)

    def update_date(self):
        wallet = self.new_response['wallet']
        values_list = wallet['transactions']
        for _dict in values_list:
            if 'modified' in _dict.keys():
                if type(_dict['modified']) == str:
                    _dict['modified'] = parser.parse(_dict['modified'])

        wallet.update(transactions=values_list)
        self.new_response.update(wallet=wallet)

    def test_get_orders_calls_get_orders_result(self):
        self.update_date()
        self.mock_service.return_value = self.new_response
        instance = PaymentService(url='http:3000/localhost/payments/wallet')
        result = instance.get_orders("besidoneo", "payed")
        self.mock_service.assert_called_once_with(
            'besidoneo', 'payed', "orders")
        self.assertResponse(gateway_response, result)

    def test_asserts_that_construct_query_returns_correct_values(self):
        instance = PaymentService(url='http:3000/localhost/payments/wallet')
        result = instance.construct_query("besidoneo", "paid", "orders")
        transaction_fields = [
            'created', 'to_string', 'transaction_type', 'total',
            {'fields': ['get_absolute_url'],
             'name':'booking', 'key':None}
        ]
        self.assertEqual(result, {
            'value': "besidoneo",
            'fields': ["total_used_to_hire", "total_in_session", "total_credit_used", "total_paid_to_tutor",
                       {
                           'fields': transaction_fields,
                           'name': 'transactions',
                           'key': 'filter_by',
                           'value': "paid",
                       }],
            'name': 'wallet',
            'key': 'username'
        })
        result = instance.construct_query("besidoneo", "earned", "revenues")
        self.assertEqual(result, {
            'value': "besidoneo",
            'fields': ["total_earned", "total_withdrawn",
                       "total_used_to_hire", "total_bonus_credit", "amount_available", 'count', "total_paid_to_tutor",

                       "upcoming_earnings", "amount_in_session", "total_in_session",
                       {
                           'fields': transaction_fields,
                           'name': 'transactions',
                           'key': 'filter_by',
                           'value': "earned",
                       }],
            'name': 'wallet',
            'key': 'username'
        })

    def test_filter_by_param_is_empty(self):
        pass


class PaymentServiceRpcTestCase(BaseApiTestCase):

    def setUp(self):
        self.patcher = mock.patch('gateway_client.payments.ClusterRpcProxy')
        self.mock_cluster_proxy = self.patcher.start()
        self.broker_url = "amqp://guest:guest@localhost"
        self.mock_instance = self.mock_cluster_proxy.return_value
        self.service = PaymentService('http://localhost:3000', self.broker_url)

    def tearDown(self):
        self.patcher.stop()

    def test_call_to_total_amount_earned_passes_the_correct_argument(self):
        broker_url = "amqp://guest:guest@localhost"
        PaymentService.total_amount_earned_by_tutors(broker_url)
        self.mock_cluster_proxy.assert_called_once_with(
            {'AMQP_URI': broker_url})
        self.mock_instance.__enter__(
        ).payment_service.get_total_tutor_earnings.assert_called_once_with()

    def test_update_amount_available_is_called(self):
        self.service.update_amount_available('james', "-")
        self.mock_instance.__enter__(
        ).payment_service.update_amount_available.async.assert_called_once_with(
            username='james', default='-'
        )

    def test_update_paystack_auth_code(self):
        self.service.update_paystack_auth_code("james", "hello")
        self.mock_instance.__enter__(
        ).payment_service.update_paystack_auth_code.async.assert_called_once_with(
            username='james', auth_code="hello"
        )

    def test_trigger_payment(self):
        self.service.trigger_payment(
            "james", 'Bank', ch=False, auto_withdraw=True)
        self.mock_instance.__enter__(
        ).payment_service.trigger_payment.assert_called_once_with(
            username="james", payment_type='Bank', ch=False, auto_withdraw=True
        )
