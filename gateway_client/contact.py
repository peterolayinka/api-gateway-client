import requests
import os
from . import TuteriaApiException
import json


class SendGridAPIHelper(object):

    def __init__(self, url):
        self.EMAIL_SERVICE_URL = url
        if not url:
            self.EMAIL_SERVICE_URL = os.getenv('EMAIL_SERVICE_URL', '')
            if not self.EMAIL_SERVICE_URL:
                raise TuteriaApiException(
                    'Url for Email Service was not found. Please pass in a url through "url" parameter')



    def create_contact_recipient(self, email, first_name, last_name, **customfields):

        data = dict(email=email, first_name=first_name,
            last_name=last_name
        )
        if customfields:
            data.update(customfields)

        return self.create_sendgrid_contact_recipients([data])

    def create_sendgrid_contact_recipients(self, data):

        res = requests.post(
            '{}/create_contact_recipient/'.format(self.EMAIL_SERVICE_URL), data=json.dumps(data)
        )

        contact = res.json()['data']

        if int(contact['error_count']) > 0:

            raise TuteriaApiException(str(contact['errors'])[1:-1])

        return contact['persisted_recipients']

