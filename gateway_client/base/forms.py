
class ModelSerializerMixin(object):
    @classmethod
    def form_dict(cls):
        return cls.get_form().serialized_form_fields()

    @classmethod
    def get_form(cls):
        return cls.form


class RootFormMixin(object):

    @classmethod
    def serialized_form_fields(cls):
        from django import forms
        instance = cls()
        result = {}
        for key, field in instance.fields.items():
            value = {}
            value['type'] = field.__class__.__name__
            if value['type'] == 'LazyTypedChoiceField':
                value.update(type='ChoiceField')
            if isinstance(field.widget, forms.HiddenInput):
                value['hidden'] = True
            kwargs = {}
            kwargs['required'] = field.required
            if hasattr(field, 'choices'):
                kwargs.update(choices=field.choices)
            if isinstance(field.widget, forms.SelectDateWidget):
                the_class = field.widget.__class__
                kwargs['widget'] = the_class.__name__
                kwargs['kwargs'] = {'years': field.widget.years}
            if value['type'] == 'SimpleArrayField':
                kwargs['base_field'] = field.base_field.__class__.__name__
            if field.label:
                kwargs['label'] = field.label
            value.update(kwargs=kwargs)
            value.update(error_messages=field.error_messages)
            result[key] = value
        return result

    def after_save(self, action_params):
        instance = self.save()
        for key, value in action_params.items():
            setattr(instance, key, value)
        instance.save()
        return instance


class DynamicFormMixin(object):
    
    detail_key = 'request_slug'

    def __init__(self, service, *args, **kwargs):
        self.request_slug = kwargs.pop(self.detail_key, None)
        super(DynamicFormMixin, self).__init__(*args, **kwargs)
        self.service = service
        self.extra_form_args = {}
        FormField = self.service.get_form_parameters(**self._get_extra_kwargs())
        self.initialize_fields(FormField)

    def _get_extra_kwargs(self):
        """Handles the scenario when a detail item needs to be validated
        self.request_slug represents the detail_key/id"""
        dicto = self.extra_form_args
        if self.request_slug:
            dicto.update(**{self.detail_key: self.request_slug})
        return dicto

    def service_clean(self, data):
        """this function returns the data that would be
        sent to the service for validation"""
        return data

    def pre_save_form(self):
        pass

    def get_validation_results(self, data):
        new_data = self.service_clean(data)
        return self.service.validate(
            new_data, **self._get_extra_kwargs())

    def clean(self):
        cleaned_data = super(DynamicFormMixin, self).clean()
        data = cleaned_data.copy()
        validations = self.get_validation_results(data)
        self._errors = {}
        for field, value in validations['errors'].items():
            self.add_error(field, value[0])
        return cleaned_data

    def save(self, **kwargs):
        self.cleaned_data.update(post_action=kwargs)
        self.pre_save_form()
        instance = self.service.save_form(
            self.cleaned_data, **self._get_extra_kwargs())
        return instance
