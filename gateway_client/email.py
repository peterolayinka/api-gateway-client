import requests
import json
import os
from . import TuteriaApiException, TuteriaDetail


def email_and_sms_helper(booking=None, sms_options=None, backend="mailgun_backend",
                         client_type='infobib', from_mail="Tuteria <automated@tuteria.com>", url=None):
    aa = url
    if not url:
        aa = os.getenv('EMAIL_SERVICE_URL', '')
        if not aa:
            raise TuteriaApiException(
                'Url for Email Service was not found. Please pass in a url through "url" parameter')
    api_data = {}
    attachment = None
    if booking is not None:
        attachment = booking.pop('attachment', None)

        api_data = booking
        api_data['from_mail'] = from_mail
        # if os.getenv("DJANGO_CONFIGURATION", "") == "StagingDev":
        #     api_data['to'] = "gbozee@gmail.com"
        emails = api_data['to']
        api_data['to'] = [emails]
        if attachment:
            api_data['to'] = emails
        a = backend
        if not isinstance(backend, str):
            a = "mailgun_backend"
        api_data['backend'] = a
    if sms_options:
        sms_data = dict(
            receiver=sms_options['receiver'],
            body=sms_options['body'],
            client_type=client_type
        )
        if os.getenv("DJANGO_CONFIGURATION", "") == "StagingDev":
            sms_data['receiver'] = "+2348128800809"
        api_data.update(sms_options=sms_data)
    if attachment:
        params = dict(data=api_data, files={"attachment": attachment})
    else:
        params = dict(json=api_data)
    req = requests.post('{}/send_message/'.format(aa), **params)
    req.raise_for_status()
    return req.json()
