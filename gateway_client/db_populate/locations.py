import importlib
from . import BaseTransferClient, populate
from django.db import transaction


def populate_all_data(settings,no_to_populate=None, import_path="location_service.location.models"):
    models = importlib.import_module(import_path)
    
    class ConstituencyTransferClient(BaseTransferClient):
        model = models.Constituency
        db_name = "users_constituency"
        exclude_fields = ['constituency', 'location', 'related_with']

        def populate_related_with(self):
            query = [d for d in self.get_query()]
            for rec in query:
                self.model.objects.filter(
                    pk=rec.id).update(related_with_id=rec.related_with_id)
            print("{} Dump Completed".format(self.model.__class__.__name__))

    class LocationTransferClient(BaseTransferClient):
        model = models.Location
        db_name = "users_location"
        user_id = 'user_id'
        exclude_fields = ['userwithlocation', 'get_country', 'area', 'region']

        def get_sql_query(self):
            return ('SELECT * from {0} '
                    'INNER JOIN "users_userprofile" ON ("{0}".{1} = "users_userprofile".{1}) '
                    'where "users_userprofile".application_status > 0 '
                    ' ORDER BY "users_userprofile".{1} ASC'.format(self.db_name, self.user_id))

        def populate_data(self):
            from django.db import IntegrityError
            fields = self.get_fields()
            #Cant use a bulk create: Multi-table models are not supported.
            for d in self.get_query():
                #Put each inside a transaction.
                with transaction.atomic():
                    location = populate(self.model, fields, d)
                    location.save()
                    location.save_location_for_user(getattr(d, self.user_id))
            print("{} Dump Completed".format(self.model.__class__.__name__))


    pp = ConstituencyTransferClient(settings.HOST_URL, no_to_populate=no_to_populate)
    pp.populate_data()
    pp.populate_related_with()
    ll = LocationTransferClient(settings.HOST_URL, no_to_populate=no_to_populate)
    ll.populate_data()
    
